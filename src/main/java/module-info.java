module org.nzhinuSoft {
    requires javafx.controls;
    requires javafx.fxml;
    /*requires jfxrt;
    requires rt;*/

    opens org.nzhinuSoft to javafx.fxml;
    exports org.nzhinuSoft;
}
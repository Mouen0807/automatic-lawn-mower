package org.nzhinuSoft;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.io.FileNotFoundException;

public class LawnMower extends StackPane {

    private int intValue;
    private int xposition;
    private int yposition;
    private Rectangle cadre;
    private String orientation;
    private boolean isStopped;


    ImageView imageView=new ImageView(new Image(getClass().getResourceAsStream("images/direction.png")));


    public LawnMower(double width, double height)  {


        this.isStopped=false;
        this.cadre=new Rectangle();
        this.cadre.setWidth(width-45);
        this.cadre.setHeight(height-45);
        imageView.setFitWidth(width-45);
        imageView.setFitHeight(height-45);
        changeColor();
        super.setWidth(width-45);
        super.setHeight(height-45);
        super.getChildren().addAll(cadre,imageView);



    }

    public boolean isStopped() {
        return isStopped;
    }

    public void setStopped(boolean stopped) {
        isStopped = stopped;
    }

    void setOrientationImageView(){
        switch(getOrientation()) {
            case "N":
                imageView.setRotate(180);
                break;
            case "S":
                imageView.setRotate(0);
                break;
            case "W":
                imageView.setRotate(90);
                break;
            case "E":
                imageView.setRotate(270);
                break;
            default:
                break;
        }
    }

    public void changeColor() // change la couleur du cadre
    {
        cadre.setFill(Color.BLUE);
    }

    public int getXposition() {
        return xposition;
    }

    public void setXposition(int xposition) {
        this.xposition = xposition;
    }

    public int getYposition() {
        return yposition;
    }

    public void setYposition(int yposition) {
        this.yposition = yposition;
    }


    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }

    public Rectangle getCadre() {
        return cadre;
    }

    public void setCadre(Rectangle cadre) {
        this.cadre = cadre;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
        setOrientationImageView();
    }
}
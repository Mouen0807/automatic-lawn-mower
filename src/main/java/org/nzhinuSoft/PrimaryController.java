package org.nzhinuSoft;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.*;

public class PrimaryController implements Initializable {

    @FXML
    private Button buttonStart;

    @FXML
    private Button buttonClear;

    @FXML
    private TextArea inputTextArea;

    @FXML
    private TextArea outputTextArea;

    @FXML
    private GridPane grid;

    private static int numCols = 10;
    private static int numRows = 10;
    private double widthOfCells;
    private double heightOfCells;
    private final static int TIME_TRANSITION=1500;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //createGrid();

    }

    @FXML
    void onClickButtonClear(ActionEvent event)  {

        removeFromGrid();
        outputTextArea.setText("");
        inputTextArea.setText("");
        buttonStart.setDisable(false);
        buttonClear.setDisable(true);


    }

    void removeFromGrid(){
        while(grid.getChildren().size()>1){
            grid.getChildren().remove(1);
        }
        while(grid.getRowConstraints().size()>0){
            grid.getRowConstraints().remove(0);
        }
        while(grid.getColumnConstraints().size()>0){
            grid.getColumnConstraints().remove(0);
        }

    }

    public Node getLawnMowerByRowColumnIndex (final int row, final int column) {
        
        for (Node node : grid.getChildren())
                if (GridPane.getColumnIndex(node) != null && GridPane.getRowIndex(node) != null
                        && GridPane.getRowIndex(node) == numRows-column
                        && GridPane.getColumnIndex(node) == row)
                    return node;
            return null;

    }

    void SetRowIndex(LawnMower lawnMower, int i){
        Node node=getLawnMowerByRowColumnIndex(i,lawnMower.getYposition());
        if(node==null && (i<=numCols && i>=0)){
            GridPane.setColumnIndex( lawnMower, i);
            lawnMower.setXposition(i);
        }
        else{
            if(node!=null){
                outputTextArea.setText(outputTextArea.getText()+"Collision ");
            }
            if(i>numCols || i<=-1){
                outputTextArea.setText(outputTextArea.getText()+"\nEffet de bord ");
            }
            lawnMower.setStopped(true);
        }
    }

    void SetColumnIndex(LawnMower lawnMower, int j){

        Node node=getLawnMowerByRowColumnIndex(lawnMower.getXposition(),j);
        if(node==null && j<=numRows && j>=0){
            GridPane.setRowIndex( lawnMower, numRows-j);
            lawnMower.setYposition(j);
        }
        else{
            if(node!=null){
                outputTextArea.setText(outputTextArea.getText()+"Collision ");
            }
            if(j>numRows ){/*j<=-1*/
                outputTextArea.setText(outputTextArea.getText()+"\nEffet de bord ");
            }
            lawnMower.setStopped(true);
        }

    }

    void createGrid(int numCols,int numRows){

        numCols++;
        numRows++;
        removeFromGrid();
        double gridHeight=grid.getHeight(),gridWidth=grid.getWidth();
        double widthOfCells=gridWidth/numCols;
        double heightOfCells=gridHeight/numRows;

        ColumnConstraints cc = new ColumnConstraints();
        cc.setPercentWidth(widthOfCells);

        for (int i = 0; i < numCols; i++) {
            grid.getColumnConstraints().add(cc);
        }

        RowConstraints rc = new RowConstraints();
        rc.setPercentHeight(heightOfCells);

        for (int i = 0; i < numRows; i++) {
            grid.getRowConstraints().add(rc);
        }
    }

    void updateWidthOfCellsAndHeightOfCells(){
        widthOfCells=grid.getWidth()/numCols;
        heightOfCells=grid.getHeight()/numRows;
    }


    @FXML
    void onClickButtonStart(ActionEvent event)  {


        String s=inputTextArea.getText();
        String [] arrayData=s.split("\n");
        numRows=Character.getNumericValue(arrayData[0].charAt(0));
        numCols=Character.getNumericValue(arrayData[0].charAt(2));
        createGrid(numCols,numRows);
        updateWidthOfCellsAndHeightOfCells();

        int i=1;
        int afterTime=0;// milliseconds
        while(i < arrayData.length){
            String position=arrayData[i];
            String path=arrayData[i+1];
            LawnMower lawnMower=new LawnMower(widthOfCells,heightOfCells);
            simulatePath(lawnMower,position,path,afterTime);
            i+=2;
            afterTime+=TIME_TRANSITION*(path.length()+2);

        }
        buttonClear.setDisable(false);
        buttonStart.setDisable(true);
    }

    void changeOrientation(String direction,LawnMower lawnMower){
        switch(direction){
            case "D":
                switch(lawnMower.getOrientation()) {
                    case "N":
                        lawnMower.setOrientation("E");
                        break;
                    case "S":
                        lawnMower.setOrientation("W");
                        break;
                    case "W":
                        lawnMower.setOrientation("N");
                        break;
                    case "E":
                        lawnMower.setOrientation("S");
                        break;
                    default:
                        break;
                }
                break;
            case "G":
                switch(lawnMower.getOrientation()) {
                    case "N":
                        lawnMower.setOrientation("W");
                        break;
                    case "S":
                        lawnMower.setOrientation("E");
                        break;
                    case "W":
                        lawnMower.setOrientation("S");
                        break;
                    case "E":
                        lawnMower.setOrientation("N");
                        break;
                    default:
                        break;
                }
                break;

            default:
                break;
        }
    }

    void moveFromPosition(String orientation, int i,int j ,LawnMower lawnMower){
        switch(orientation){
            case "N":
                SetColumnIndex(lawnMower,j+1);
                break;
            case "S":
                SetColumnIndex(lawnMower,j-1);
                break;
            case "W":
                SetRowIndex(lawnMower,i-1);
                break;
            case "E":
                SetRowIndex(lawnMower,i+1);
                break;
            default:
                break;
        }
    }

    void addPosition(LawnMower lawnMower, int i, int j){
        this.grid.add(lawnMower, i,numRows-j);
    }

    void simulatePath(LawnMower lawnMower,String position,String path,int afterTime) {

        int initialX=Character.getNumericValue(position.charAt(0));
        int initialY=Character.getNumericValue(position.charAt(2));
        String initialOrientation=Character.toString(position.charAt(4));
        lawnMower.setXposition(initialX);
        lawnMower.setYposition(initialY);
        lawnMower.setOrientation(initialOrientation);
        Timer timer;
        timer=new Timer("Essai");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {//Update UI from thread
                    @Override
                    public void run() {
                        addPosition(lawnMower,initialX,initialY);
                    }
                });

            }
        },(TIME_TRANSITION/2)+afterTime);



        int n=path.length();
        for(int i=0;i<n;i++){
            if(!lawnMower.isStopped()){
                timer =new Timer ("new operations");
                int finalI = i;
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(path.charAt(finalI)=='A'){
                            moveFromPosition(lawnMower.getOrientation(),lawnMower.getXposition(),lawnMower.getYposition(), lawnMower);
                        }
                        else{
                            changeOrientation(Character.toString(path.charAt(finalI)),lawnMower);
                        }
                    }
                },TIME_TRANSITION*(i+1)+afterTime);

            }
        }
        timer=new Timer("new screen output");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                outputTextArea.setText(outputTextArea.getText()+lawnMower.getXposition() +" "+lawnMower.getYposition()+" "+lawnMower.getOrientation()+"\n");
            }
        },TIME_TRANSITION*(n+1)+afterTime);

    }



}
